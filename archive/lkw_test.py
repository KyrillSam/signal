import numpy as np
from sklearn import svm
from sklearn.metrics import classification_report, accuracy_score
import cv2
import constants
import os
import shutil


# read sample data cars left_line
lkw_left, amount_lkw, _ = constants.read_data('LeftSamples/LKW_Samples/LKW/')
# read sample data nothing left_line
rest_left, amount_rest, _ = constants.read_data('LeftSamples/LKW_Samples/Rest')
# merge data
data = lkw_left + rest_left

# HOG
hog_features = constants.use_hog(data)

# SVM
# SVM Initial
lkw_clf = svm.SVC(kernel='linear', C=1E10)
# Labels for training
label_lkw = ['L'] * amount_lkw
label_rest = ['R'] * amount_rest
labels = np.concatenate((label_lkw, label_rest), axis=0)
labels = np.array(labels).reshape(len(labels), 1)
# print(labels)

data_frame = np.hstack((hog_features, labels))
np.random.shuffle(data_frame)

# Seperate Data training and Test
size = round(len(data) * 0.8)
x_train = data_frame[:size, :-1]
y_train = data_frame[:size, -1:].ravel()

lkw_clf.fit(x_train, y_train)
x_test = data_frame[size:, :-1]
y_test = data_frame[size:, -1:].ravel()

y_pred = lkw_clf.predict(x_test)

print("Accuracy: " + str(accuracy_score(y_test, y_pred)))
print('\n')
print(classification_report(y_test, y_pred))

os.chdir('F:\\17343\\')
data, _, paths = constants.read_data('Samples/')

print('scanned %s pictures for lkw' % len(data))

hog_features = constants.use_hog(data)
print('hog done')

y_pred = lkw_clf.predict(hog_features)
print('pred done')

#shutil.rmtree('Samples')
os.mkdir('LKW')
os.mkdir('LKW_Rest')
for i in range(len(y_pred)):
    if y_pred[i] == 'L':
        shutil.copy(paths[i], 'LKW/sample%s.png' % i)
    if y_pred[i] == 'R':
        shutil.copy(paths[i], 'LKW_Rest/sample%s.png' % i)