import numpy as np
from sklearn import svm
from sklearn.metrics import classification_report, accuracy_score
import cv2
import constants
import os
import shutil



# read sample data cars left_line
cars_left, amount_cars, _ = constants.read_data('LeftSamples/leftline_cars/')
# read sample data nothing left_line
nothing_left, amount_nothing, _ = constants.read_data('LeftSamples/leftline_nothing/')
# merge data
data = cars_left + nothing_left

# show images
#for image in data:
#cv2.imshow('bild', image)
#cv2.waitKey()
#cv2.destroyAllWindows()

# HOG
hog_features = constants.use_hog(data)

# SVM
# SVM Initial
first_clf = svm.SVC(kernel='linear', C=1E10)
# Labels for training
label_cars = ['C'] * amount_cars
label_nothing = ['N'] * amount_nothing
labels = np.concatenate((label_cars, label_nothing), axis=0)
labels = np.array(labels).reshape(len(labels), 1)
# print(labels)

data_frame = np.hstack((hog_features, labels))
np.random.shuffle(data_frame)

# Seperate Data training and Test
size = round(len(data) * 0.8)
x_train = data_frame[:size, :-1]
y_train = data_frame[:size, -1:].ravel()

first_clf.fit(x_train, y_train)
x_test = data_frame[size:, :-1]
y_test = data_frame[size:, -1:].ravel()

y_pred = first_clf.predict(x_test)

print("Accuracy: " + str(accuracy_score(y_test, y_pred)))
print('\n')
print(classification_report(y_test, y_pred))

# Test Data for cars in the leftline
data.clear()
os.chdir('F:\\17343\\')
data, _, paths = constants.read_data('04/')
print('scanned %s pictures' % len(data))

hog_features = constants.use_hog(data)

print('hog done')
y_pred = first_clf.predict(hog_features)
print('pred done')
state = 'searching'
barrier = 150
for idx in range(len(data)):
    if y_pred[idx] == 'C' and state == 'searching':
        state = 'found'
    elif y_pred[idx] == 'C' and state == 'found':
        y_pred[idx] = 'N'
    elif y_pred[idx] == 'N' and state == 'found':
        state = 'searching'
#shutil.rmtree('Samples')
os.mkdir('Samples')
for i in range(len(y_pred)):
    if y_pred[i] == 'C':
        #cv2.imshow('bild', data[i])
        #cv2.waitKey()
        #cv2.destroyAllWindows()
        #cv2.imwrite('Samples/sample%s.png' % i, data[i])
        shutil.copy(paths[i], 'Samples/sample%s.png' % i)

