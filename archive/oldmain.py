import numpy as np
from sklearn import svm
from sklearn.metrics import classification_report, accuracy_score
import cv2
import constants
import os
import shutil



# read sample data cars left_line
cars_left, amount_cars, _ = constants.read_data('LeftSamples/leftline_cars/')
# read sample data nothing left_line
nothing_left, amount_nothing, _ = constants.read_data('LeftSamples/leftline_nothing/')
# merge data
data = cars_left + nothing_left

# show images
#for image in data:
 #cv2.imshow('bild', image)
 #cv2.waitKey()
 #cv2.destroyAllWindows()

# HOG
hog_features = constants.use_hog(data)

# SVM
# SVM Initial
clf = svm.SVC(kernel='linear', C=1E10)
# Labels for training
label_cars = ['C'] * amount_cars
label_nothing = ['N'] * amount_nothing
labels = np.concatenate((label_cars, label_nothing), axis=0)
labels = np.array(labels).reshape(len(labels), 1)
# print(labels)

data_frame = np.hstack((hog_features, labels))
np.random.shuffle(data_frame)

# Seperate Data training and Test
size = round(len(data) * 0.8)
x_train = data_frame[:size, :-1]
y_train = data_frame[:size, -1:].ravel()

clf.fit(x_train, y_train)
x_test = data_frame[size:, :-1]
y_test = data_frame[size:, -1:].ravel()

y_pred = clf.predict(x_test)

print("Accuracy: " + str(accuracy_score(y_test, y_pred)))
print('\n')
print(classification_report(y_test, y_pred))

# Test Data for cars in the leftline
data.clear()
os.chdir('F:\\17343\\')
data, _, paths = constants.read_data('04/')
print('scanned %s pictures' % len(data))

hog_features = constants.use_hog(data)

print('hog done')
y_pred = clf.predict(hog_features)
print('pred done')

# shutil.rmtree('Carsleft')
os.mkdir('Carsleft')
for i in range(len(y_pred)):
    if y_pred[i] == 'C':
        shutil.copy(paths[i], 'Carsleft/sample%s.png' % i)
