import os
import cv2
import constants


color = (0, 0, 0)
thickness = 2
source_path ='C:/Users/Kyrill/Project/signal/RightSamples/right_cars'
output_path = source_path+'/copy'
os.mkdir(output_path)
data=[]
os.chdir(source_path)
for filename in os.listdir(source_path):
    if os.path.isfile(os.path.join(source_path, filename)):
        img = cv2.imread(os.path.join(source_path, filename), 0)
        cv2.rectangle(img, (constants.RIGHTLINE_MINX_S, constants.RIGHTLINE_MINY_S), (640, constants.RIGHTLINE_MAXY_S),
                      color, thickness)
        cv2.rectangle(img, (constants.MIDLINE_MINX_S, constants.MIDLINE_MINY_S),
                      (constants.MIDLINE_MAXX_S, constants.MIDLINE_MAXY_S), color, thickness)
        cv2.rectangle(img, (constants.LEFTLINE_MINX_S, constants.LEFTLINE_MINY_S),
                      (constants.LEFTLINE_MAXX_S, constants.LEFTLINE_MAXY_S), color, thickness)
        cv2.imwrite(output_path+'/'+filename, img)