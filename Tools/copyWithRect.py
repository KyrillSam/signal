import os
import cv2
import constants


color = (0, 0, 0)
thickness = 2
source_path ='/home/calesto/AMSA/17343/Left_Line_Cars'
output_path = source_path+'/copyBig'
os.mkdir(output_path)
data=[]
os.chdir(source_path)
for filename in os.listdir(source_path):
    if os.path.isfile(os.path.join(source_path, filename)):
        img = cv2.imread(os.path.join(source_path, filename), 0)
        cv2.rectangle(img, (constants.RIGHTLINE_MINX, constants.RIGHTLINE_MINY), (640, constants.RIGHTLINE_MAXY),
                      color, thickness)
        cv2.rectangle(img, (constants.MIDLINE_MINX, constants.MIDLINE_MINY),
                      (constants.MIDLINE_MAXX, constants.MIDLINE_MAXY), color, thickness)
        cv2.rectangle(img, (constants.LEFTLINE_MINX, constants.LEFTLINE_MINY),
                      (constants.LEFTLINE_MAXX, constants.LEFTLINE_MAXY), color, thickness)
        cv2.imwrite(output_path+'/'+filename, img)