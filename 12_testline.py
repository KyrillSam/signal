import filter
import constants
import os
import cv2


dir_stick = 'C:\\Users\\Kyrill\\Project\\signal'
folder_to_save_left_cars = 'Left_Line_Cars'
folder_to_save_mid_cars = 'Mid_Line_Cars'
curr_dir = os.getcwd()
os.chdir(dir_stick)
data, path = constants.list_images('12/')
print('loaded images')
l, m, r = constants.crop_images(data, True)
# for pic in m:
   # cv2.imshow('pic', pic)
   # cv2.waitKey()
   # cv2.destroyAllWindows()
data = []
print('sliced images')
os.chdir(curr_dir)


filter.load_samples('LeftSamples/left_cars/', 'LeftSamples/left_nothing/', 'l', dir_stick,  '04/',
                    folder_to_save_left_cars, True, l, path)
filter.load_samples('MidSamples/Mid_cars/', 'MidSamples/Mid_nothing/', 'm', dir_stick,  '04/',
                    folder_to_save_mid_cars, True, m, path)
filter.load_samples('LeftSamples/LKW_Samples/LKW/', 'LeftSamples/LKW_Samples/Rest', 'l', dir_stick,
                    folder_to_save_left_cars, 'LKW')
