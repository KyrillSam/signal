import cv2
import os
from skimage.feature import hog
import numpy as np
import shutil

#Cropping
WIN_WITH = 280;
WIN_HIGHT = 200;

LEFTLINE_MAXY = 350
LEFTLINE_MINY = LEFTLINE_MAXY-WIN_HIGHT
LEFTLINE_MINX = 20
LEFTLINE_MAXX = LEFTLINE_MINX+WIN_WITH

LEFTLINE_MAXY_S = 350
LEFTLINE_MINY_S = 300
LEFTLINE_MINX_S = 20
LEFTLINE_MAXX_S = 250

MIDLINE_MAXY = 350
MIDLINE_MINY = MIDLINE_MAXY-WIN_HIGHT
MIDLINE_MINX = 265
MIDLINE_MAXX = MIDLINE_MINX+WIN_WITH

MIDLINE_MAXY_S = 350
MIDLINE_MINY_S = 300
MIDLINE_MINX_S = 265
MIDLINE_MAXX_S = MIDLINE_MINX+WIN_WITH

RIGHTLINE_MINY = 30
RIGHTLINE_MAXY = RIGHTLINE_MINY+WIN_HIGHT
RIGHTLINE_MINX = 480

RIGHTLINE_MINY_S = 190
RIGHTLINE_MAXY_S = 250
RIGHTLINE_MINX_S = 480

SCALLING = (WIN_WITH, WIN_HIGHT)

#HOG
PPC = 48
CELLS = 2


def read_data(path, crop, first):
    images, paths = list_images(path)
    if crop == 'l':
        new_images = crop_leftlane(images, first)
    elif crop == 'm':
        new_images = crop_midlane(images, first)
    elif crop == 'r':
        new_images = crop_rightlane(images, first)
    return new_images, len(new_images), paths


def crop_images(images, first):
    l = crop_leftlane(images, first)
    m = crop_midlane(images, first)
    r = crop_rightlane(images, first)
    return l, m, r


# Method for croping the right line of the image
# Input: the road raw image array to crop
# Output: the right line of the image array
def crop_leftlane(images, first):
    croped_img = []
    if first:
        for idx in range(len(images)):
            croped_img.append(images[idx][LEFTLINE_MINY_S:LEFTLINE_MAXY_S,
                              LEFTLINE_MINX_S:LEFTLINE_MAXX_S])
    else:
        for idx in range(len(images)):
            croped_img.append(images[idx][LEFTLINE_MINY:LEFTLINE_MAXY,
                              LEFTLINE_MINX:LEFTLINE_MAXX])
    return croped_img


# Method for croping the right line of the image
# Input: the road raw image array to crop
# Output: the right line of the image array
def crop_midlane(images, first):
    croped_img = []
    if first:
        for idx in range(len(images)):
            croped_img.append(images[idx][MIDLINE_MINY_S:MIDLINE_MAXY_S,
                              MIDLINE_MINX_S:MIDLINE_MAXX_S])
    else:
        for idx in range(len(images)):
            croped_img.append(images[idx][MIDLINE_MINY:MIDLINE_MAXY,
                              MIDLINE_MINX:MIDLINE_MAXX])
    return croped_img


# Method for croping the right line of the image
# Input: the road raw image array to crop
# Output: the right line of the image array
def crop_rightlane(images,first):
    croped_img = []
    if first:
        for idx in range(len(images)):
            croped_img.append(images[idx][RIGHTLINE_MINY_S:RIGHTLINE_MAXY_S,
                              RIGHTLINE_MINX_S:])
    else:
        for idx in range(len(images)):
            croped_img.append(images[idx][RIGHTLINE_MINY:RIGHTLINE_MAXY,
                              RIGHTLINE_MINX:])
    return croped_img


# Collect Data from folder and subfolders into 'temp_data' as array
# Input: folder location
# Output: Array 'temp_data', each temp_data[x] is a picture
def list_images(path):
    temp_data = []
    temp_path = []
    search_dir(path, temp_data, temp_path)
    return temp_data, temp_path


def search_dir(path, temp_data, temp_path):
    for filename in sorted(os.listdir(path)):
        if os.path.isfile(os.path.join(path, filename)):
            img = cv2.imread(os.path.join(path, filename), 0)
            temp_data.append(img)
            temp_path.append(os.path.join(path, filename))
        else:
            search_dir(os.path.join(path, filename), temp_data, temp_path)


def use_hog(data, first):
    hog_features = []
    if first:
        ppc = 16
    else:
        ppc = 48
    for image in data:
        fd = hog(image, orientations=12, pixels_per_cell=(ppc, ppc), cells_per_block=(CELLS, CELLS),
                            block_norm='L2'
                            , visualize=False)
        hog_features.append(fd)
    return np.array(hog_features)


def save_images_lined(l, m, r, pl, pm, pr):
    paths = ['Left', 'Mid', 'Right']
    for path in paths:
        try:
            shutil.rmtree(path)
        except:
            print('%s does not exist' % path)
        os.mkdir(path)
        images_lined = []
        paths_lined = []
        if path == 'Left':
            images_lined = l
            paths_lined = pl
        elif path == 'Mid':
            images_lined = m
            paths_lined = pm
        elif path == 'Right':
            images_lined = r
            paths_lined = pr
        for i in range(len(images_lined)):
            shutil.copy(paths_lined[i], '%s/sample%s.png' % (path, i))