import numpy as np
from sklearn import svm
from sklearn.metrics import classification_report, accuracy_score
import cv2
import constants
import os
import shutil
from time import sleep


# filter data with templates
# Input: Dir of samples for True(String) , Dir of samples for False(String), how to crop (l)eft, (m)iddle or (r)ight,
# where is the data to check with(String), folder with the data(String), where to save filtered images (String)
# optional: check only for cars no mater which,
# matrix of pictures instead of reading them out, and array of paths of those images
# Output: no Output, but filtered pictures in data_dir/new_dir
def load_samples(true_samples_path, false_samples_path, crop, data_dir, folder, new_dir, first=False, matrix=[],
                 matrix_path=[]):

    curr_dir = os.getcwd()
    # read sample data cars left_line
    true_samples, amount_true, _ = constants.read_data(true_samples_path, crop, first)
    # read sample data nothing left_line
    false_samples, amount_false, _ = constants.read_data(false_samples_path, crop, first)
    # merge data
    data = true_samples + false_samples
    # HOG
    hog_features = constants.use_hog(data, first)
    # SVM Initial
    clf = svm.SVC(kernel='linear', C=1E10)
    # Labels for training
    label_true = ['T'] * amount_true
    label_false = ['F'] * amount_false
    labels = np.concatenate((label_true, label_false), axis=0)
    labels = np.array(labels).reshape(len(labels), 1)
    data_frame = np.hstack((hog_features, labels))
    np.random.shuffle(data_frame)

    # Separate Data training and Test
    size = round(len(data) * 1)
    x_train = data_frame[:size, :-1]
    y_train = data_frame[:size, -1:].ravel()

    clf.fit(x_train, y_train)
    #x_test = data_frame[size:, :-1]
    #y_test = data_frame[size:, -1:].ravel()

#    y_pred = clf.predict(x_test)

    #print("Accuracy: " + str(accuracy_score(y_test, y_pred)))
    #print('\n')
    #print(classification_report(y_test, y_pred))

    # Test Data for cars in the left line
    os.chdir(data_dir)
    if first:
        data = matrix
        paths = matrix_path
    else:
        data.clear()
        data, _, paths = constants.read_data(folder, crop, first)
        print('scanned %s pictures' % len(data))

    hog_features = constants.use_hog(data,first)

    print('hog done')
    y_pred = clf.predict(hog_features)
    print('pred done')

    try:
        shutil.rmtree(new_dir)
    except:
        print('%s does not exist' % new_dir)
    sleep(1)
    os.mkdir(new_dir)
    numberOld = 0

    if crop is 'l':
        timeIntervall = 0.8
    elif crop is 'm':
        timeIntervall = 0.5
    else:
        timeIntervall = 0.4

    print("Timeintervall ***************** " + str(timeIntervall))
    for i in range(len(y_pred)):
        if y_pred[i] == 'T':
            s = paths[i]
            size = len(s)

            #Checks if the timestamp is >= 500
            numberNew = 60*float(s[-13:-12])+float(s[-10:-4])
            #print(numberNew)
            #print(s[-13:-12])
            if abs(numberOld - numberNew) >= timeIntervall:
                #print("new vehicle")
                #print(s[size - 13] + s[size - 12] + '_' + s[size - 10:size])
                shutil.copy(paths[i], new_dir + '/' + s[size - 13] + s[size - 12] + '_' + s[size - 10:size])
                numberOld = numberNew




            #shutil.copy(paths[i], '%s/sample%s.png' % (new_dir, i))

    os.chdir(curr_dir)


