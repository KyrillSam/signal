import filter
import constants
import os

readData = True
l, m, r = [], [], []


directions = ['E:/Projekt/','/home/calesto/AMSA/17343/']
dir_stick = directions[0]
folder_to_save_left_cars = 'Left_Line_Cars'
folder_to_save_mid_cars = 'Mid_Line_Cars'
folder_to_save_right_cars = 'Right_Line_Cars'
curr_dir = os.getcwd()

"""control"""
left, mid, right = False, False, True


os.chdir(dir_stick)
if readData:
    data, path = constants.list_images('04/')
    print('loaded images')
    l, m, r = constants.crop_images(data, True)
    del data
print('sliced images')
os.chdir(curr_dir)

if readData:
    if left:
        #Vehicle Detection
        print("left Vehicle Detectation")
        filter.load_samples('LeftSamples/left_cars/', 'LeftSamples/left_nothing/', 'l', dir_stick,  '04/',
                            folder_to_save_left_cars, True, l, path)
        del l

    if mid:
        print("mid Vehicle Detectation")
        filter.load_samples('MidSamples/mid_cars/', 'MidSamples/mid_nothing/', 'm', dir_stick,  '04/',
                        folder_to_save_mid_cars, True, m, path)
        del m

    if right:
        print("right Vehicle Detectation")
        filter.load_samples('RightSamples/right_cars/', 'RightSamples/right_nothing/', 'r', dir_stick,  '04/',
                        folder_to_save_right_cars, True, r, path)
        del r, path

if left:
    #LKW Classification
    print("left LKW Detectation")
    filter.load_samples('LeftSamples/LKW_Samples/LKW/', 'LeftSamples/LKW_Samples/Rest', 'l', dir_stick,
                        folder_to_save_left_cars, 'l-LKW')
    # Transporter Classification
    print("left Transporter Detectation")
    filter.load_samples('LeftSamples/Transporter_Samples/Transporter/', 'LeftSamples/Transporter_Samples/Rest', 'l',
                        dir_stick,
                        folder_to_save_left_cars, 'l-Transporter')
    # PKW Classification
    print("left PKW Detectation")
    filter.load_samples('LeftSamples/PKW_Samples/PKW/', 'LeftSamples/PKW_Samples/Rest', 'l',
                        dir_stick,
                        folder_to_save_left_cars, 'l-PKW')

if mid:
    print("mid LKW Detectation")
    filter.load_samples('MidSamples/LKW_Samples/LKW/', 'MidSamples/LKW_Samples/Rest', 'm', dir_stick,
                        folder_to_save_mid_cars, 'm-LKW')
    print("mid Transporter Detectation")
    filter.load_samples('MidSamples/Transporter_Samples/Transporter/', 'MidSamples/Transporter_Samples/Rest', 'm',
                        dir_stick,
                        folder_to_save_mid_cars, 'm-Transporter')
    print("mid PKW Detectation")
    filter.load_samples('MidSamples/PKW_Samples/PKW/', 'MidSamples/PKW_Samples/Rest', 'm',
                        dir_stick,
                        folder_to_save_mid_cars, 'm-PKW')
    print("mid Motor Detectation")
    filter.load_samples('MidSamples/Mot_Samples/Mot/', 'MidSamples/Mot_Samples/Rest', 'm',
                        dir_stick,
                        folder_to_save_mid_cars, 'm-Mot')
    print("mid Bus Detectation")
    filter.load_samples('MidSamples/Bus_Samples/Bus/', 'MidSamples/Bus_Samples/Rest', 'm',
                        dir_stick,
                        folder_to_save_mid_cars, 'm-Bus')



if right:
    print("right LKW Detectation - too bad, there are no LKW on this trail")
    # too bad, there are no LKW on the left trail (right side from cameras perspective)
    print("right PKW Detectation")
    filter.load_samples('RightSamples/PKW_Samples/PKW/', 'RightSamples/PKW_Samples/Rest', 'r',
                        dir_stick,
                        folder_to_save_right_cars, 'r-PKW')
    print("right VAN Detectation")
    filter.load_samples('RightSamples/VAN_Samples/VAN/', 'RightSamples/VAN_Samples/Rest', 'r',
                        dir_stick,
                        folder_to_save_right_cars, 'r-VAN')